﻿using Moq;
using ParkingManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Testing;
using System.Data.Entity;
using ParkingManager.Common;
using Xunit;
using ParkingManager.Services;
using ParkingManager.ViewModels;

namespace ParkingManager.Tests.Services
{
    public class ReservationServiceTest
    {
        private Mock<ApplicationDbContext> context = new Mock<ApplicationDbContext>();
        private static string userId = "userId";

        private List<ParkingSpace> spaces = new List<ParkingSpace>()
        {
            new ParkingSpace { Id = 1, Mark = "A1", VehicleType = VehicleType.Car, Reservations = new List<SpaceReservation>() },
            new ParkingSpace { Id = 2, Mark = "A2", VehicleType = VehicleType.Car, Reservations = new List<SpaceReservation>() }
        };

        private List<SpaceReservation> reservations = new List<SpaceReservation>()
        {
            new SpaceReservation { Id = 1, BeginsAt = new DateTime(2015, 10, 1), EndsAt = new DateTime(2015, 10, 15),
                                   SpaceId = 1, Space = new ParkingSpace(), UserId = userId, User = new ApplicationUser() },
            new SpaceReservation { Id = 2, BeginsAt = new DateTime(2015, 10, 1), EndsAt = new DateTime(2015, 10, 15),
                                   SpaceId = 2, Space = new ParkingSpace(), UserId = userId, User = new ApplicationUser() }
        };

        public ReservationServiceTest()
        {
            AutoMapperConfiguration.Configure();

            var spacesSet = new Mock<DbSet<ParkingSpace>>()
                                .SetupData(spaces, keys => spaces.SingleOrDefault(sp => sp.Id == (int)keys[0]));
            var reservationsSet = new Mock<DbSet<SpaceReservation>>()
                                    .SetupData(reservations, keys => reservations.SingleOrDefault(r => r.Id == (int)keys[0]));

            context.Setup(c => c.Spaces).Returns(spacesSet.Object);
            context.Setup(c => c.Reservations).Returns(reservationsSet.Object);
        }

        [Fact]
        public void AddValidReservation()
        {
            var service = new ReservationService(context.Object);

            var reservation = new NewReservationViewModel
            {
                BeginsAt = new DateTime(2015, 10, 20),
                EndsAt = new DateTime(2015, 10, 22),
                VehicleType = VehicleType.Car
            };

            bool added = service.AddReservation(reservation, userId, null);
            
            Assert.True(added);
        }

        [Theory]
        [MemberData("InvalidReservationData")]
        public void TestInvalidReservation(DateTime start, DateTime end, VehicleType type)
        {
            var service = new ReservationService(context.Object);

            var reservation = new NewReservationViewModel
            {
                BeginsAt = start,
                EndsAt = end,
                VehicleType = type
            };

            bool added = service.AddReservation(reservation, userId, null);

            Assert.False(added);
        }

        public static IEnumerable<object[]> InvalidReservationData()
        {
            yield return new object[] { new DateTime(2015, 10, 5), new DateTime(2015, 10, 10), VehicleType.Truck };
            yield return new object[] { new DateTime(2015, 10, 5), new DateTime(2015, 10, 10), VehicleType.Car };
            yield return new object[] { new DateTime(2015, 9, 28), new DateTime(2015, 10, 28), VehicleType.Car };
            yield return new object[] { new DateTime(2015, 9, 28), new DateTime(2015, 10, 5), VehicleType.Car };
            yield return new object[] { new DateTime(2015, 10, 5), new DateTime(2015, 10, 28), VehicleType.Car };
        }
    }
}

