﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Web.Configuration;
using ParkingManager.Common;

namespace ParkingManager.Services
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            return SendMail(message);
        }                

        private async Task SendMail(IdentityMessage message)
        {
            using (SmtpClient smtpClient = CreateSmtpClient())
            using (MailMessage mailMessage = CreateMessage(message))
            {
                await smtpClient.SendMailAsync(mailMessage);
            }            
        }

        private SmtpClient CreateSmtpClient()
        {
            SmtpClient smtpClient = new SmtpClient(Configuration.EmailHost, Configuration.EmailPort);
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new NetworkCredential(Configuration.EmailLogin,
                                                           Configuration.EmailPassword);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            return smtpClient;
        }

        private MailMessage CreateMessage(IdentityMessage message)
        {
            var mailMessage = new MailMessage(Configuration.EmailLogin, message.Destination);
            mailMessage.Subject = message.Subject;
            mailMessage.Body = message.Body;
            mailMessage.IsBodyHtml = true;

            return mailMessage;
        }
    }
}