﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ParkingManager.ViewModels;
using ParkingManager.Models;
using AutoMapper.QueryableExtensions;
using System.Web.Mvc;
using System.Data.Entity;
using System.Linq.Expressions;

namespace ParkingManager.Services
{
    public class ReservationService : IReservationService
    {
        private ApplicationDbContext db = null;

        public ReservationService(ApplicationDbContext context)
        {
            this.db = context;
        }

        public IList<SpaceReservationViewModel> GetUserReservations(string userId)
        {
            List<SpaceReservationViewModel> reservations = db.Reservations
                                                             .Where(r => r.UserId == userId)
                                                             .ProjectTo<SpaceReservationViewModel>()
                                                             .ToList();
            return reservations;
        }

        public bool AddReservation(NewReservationViewModel reservation, string userId, ModelStateDictionary modelState)
        {
            bool success = false;

            bool reservationPossible = CheckReservationPossibility(reservation, modelState);
            if (reservationPossible)
            {
                ParkingSpace space = GetFreeSpace(reservation.BeginsAt, reservation.EndsAt, reservation.VehicleType);
                space.Reservations.Add(new SpaceReservation
                {
                    BeginsAt = reservation.BeginsAt,
                    EndsAt = reservation.EndsAt,
                    UserId = userId
                });
                db.SaveChanges();
                success = true;
            }

            return success;
        }

        private bool CheckReservationPossibility(NewReservationViewModel reservation, ModelStateDictionary modelState)
        {
            bool possible = false;

            List<int> occupiedSpacesIds = GetOccupiedSpacesIds(reservation.BeginsAt, reservation.EndsAt, reservation.VehicleType);

            int freeSpaces = db.Spaces.Where(s => !occupiedSpacesIds.Contains(s.Id))
                               .Where(s => s.VehicleType == reservation.VehicleType)
                               .Count();

            if (freeSpaces > 0)
            {
                possible = true;
            }
            else
            {
                modelState?.AddModelError("", "There are no free places for that date range.");
            }

            return possible;
        }

        private ParkingSpace GetFreeSpace(DateTime beginsAt, DateTime endsAt, VehicleType type)
        {
            List<int> occupiedSpacesIds = GetOccupiedSpacesIds(beginsAt, endsAt, type);

            ParkingSpace space = db.Spaces.Where(s => !occupiedSpacesIds.Contains(s.Id))
                               .First();

            return space;
        }

        private List<int> GetOccupiedSpacesIds(DateTime beginsAt, DateTime endsAt, VehicleType type)
        {
            List<int> occupiedSpacesIds = db.Reservations
                                            .Include(r => r.Space)
                                            .Where(r => r.Space.VehicleType == type)
                                            .Where(r => ( r.BeginsAt >= beginsAt && r.BeginsAt <= endsAt) ||
                                                        (r.EndsAt >= beginsAt && r.EndsAt <= endsAt) ||
                                                        (r.BeginsAt <= beginsAt && r.EndsAt >= endsAt))
                                            .Select(r => r.SpaceId)
                                            .ToList();
            return occupiedSpacesIds;
        }
    }
}