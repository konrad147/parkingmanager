﻿using ParkingManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParkingManager.Services
{
    public interface IReservationService
    {
        IList<SpaceReservationViewModel> GetUserReservations(string userId);
        bool AddReservation(NewReservationViewModel reservation, string userId, ModelStateDictionary modelState);
    }
}