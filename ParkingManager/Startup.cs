﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ParkingManager.Startup))]
namespace ParkingManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
