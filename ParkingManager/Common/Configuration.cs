﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace ParkingManager.Common
{
    public static class Configuration
    {
        public static int EmailPort
        {
            get { return int.Parse(WebConfigurationManager.AppSettings["EmailPort"]); }
        }

        public static string EmailHost
        {
            get { return WebConfigurationManager.AppSettings["EmailHost"]; }
        }

        public static string EmailLogin
        {
            get { return WebConfigurationManager.AppSettings["EmailLogin"]; }
        }

        public static string EmailPassword
        {
            get { return WebConfigurationManager.AppSettings["EmailPassword"]; }
        }
    }
}