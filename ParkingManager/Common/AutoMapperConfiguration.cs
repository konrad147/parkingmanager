﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ParkingManager.Models;
using ParkingManager.ViewModels;
using AutoMapper;

namespace ParkingManager.Common
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.CreateMap<SpaceReservation, SpaceReservationViewModel>();

            Mapper.AssertConfigurationIsValid();
        }
    }
}