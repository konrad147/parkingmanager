﻿using ParkingManager.Services;
using ParkingManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ParkingManager.Controllers
{
    [Authorize]
    public class ReservationController : Controller
    {
        private IReservationService reservationService = null;

        public ReservationController(IReservationService reservationService)
        {
            this.reservationService = reservationService;
        }

        public ActionResult Create()
        {
            var vm = new NewReservationViewModel
            {
                BeginsAt = DateTime.Now,
                EndsAt = DateTime.Now.AddDays(5)
            };

            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(NewReservationViewModel vm)
        {
            if (ModelState.IsValid)
            {
                bool reservationAdded = reservationService.AddReservation(vm, User.Identity.GetUserId(), ModelState);
                if (reservationAdded)
                {
                    return RedirectToAction("UserReservations");
                }
            }

            return View(vm);
        }

        public ActionResult UserReservations()
        {
            IList<SpaceReservationViewModel> reservations = reservationService.GetUserReservations(User.Identity.GetUserId());

            return View(reservations);
        }
    }
}