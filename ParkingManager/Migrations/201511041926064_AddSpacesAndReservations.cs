namespace ParkingManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSpacesAndReservations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SpaceReservations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        SpaceId = c.Int(nullable: false),
                        BeginsAt = c.DateTime(nullable: false),
                        Ends = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ParkingSpaces", t => t.SpaceId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.SpaceId);
            
            CreateTable(
                "dbo.ParkingSpaces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Mark = c.String(nullable: false, maxLength: 10),
                        VehicleType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SpaceReservations", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SpaceReservations", "SpaceId", "dbo.ParkingSpaces");
            DropIndex("dbo.SpaceReservations", new[] { "SpaceId" });
            DropIndex("dbo.SpaceReservations", new[] { "UserId" });
            DropTable("dbo.ParkingSpaces");
            DropTable("dbo.SpaceReservations");
        }
    }
}
