namespace ParkingManager.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ParkingManager.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ParkingManager.Models.ApplicationDbContext";
        }

        protected override void Seed(ParkingManager.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Spaces.AddOrUpdate(
                s => s.Mark,
                new ParkingSpace { Mark = "A1", VehicleType = VehicleType.Car },
                new ParkingSpace { Mark = "A2", VehicleType = VehicleType.Car },
                new ParkingSpace { Mark = "A3", VehicleType = VehicleType.Car },
                new ParkingSpace { Mark = "A4", VehicleType = VehicleType.Car },
                new ParkingSpace { Mark = "B1", VehicleType = VehicleType.Truck },
                new ParkingSpace { Mark = "B2", VehicleType = VehicleType.Truck },
                new ParkingSpace { Mark = "B3", VehicleType = VehicleType.Truck },
                new ParkingSpace { Mark = "B4", VehicleType = VehicleType.Truck }
            );
        }
    }
}
