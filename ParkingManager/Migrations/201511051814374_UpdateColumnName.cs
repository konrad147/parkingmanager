namespace ParkingManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateColumnName : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.SpaceReservations", "Ends", "EndsAt");            
        }
        
        public override void Down()
        {
            RenameColumn("dbo.SpaceReservations", "EndsAt", "Ends");
        }
    }
}
