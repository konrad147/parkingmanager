﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParkingManager.Models
{    
    public class ParkingSpace
    {        
        public int Id { get; set; }

        [Required]
        [MinLength(1)]
        [MaxLength(10)]
        public string Mark { get; set; }

        public VehicleType VehicleType { get; set; }  

        public virtual ICollection<SpaceReservation> Reservations { get; set; }
    }

    public enum VehicleType
    {
        Car,
        Truck
    }
}