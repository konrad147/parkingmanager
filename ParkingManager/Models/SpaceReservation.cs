﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ParkingManager.Models
{
    public class SpaceReservation
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }

        [ForeignKey("Space")]
        public int SpaceId { get; set; }

        [Required]
        public DateTime BeginsAt { get; set; }

        [Required]
        public DateTime EndsAt { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual ParkingSpace Space { get; set; }
    }
}