﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ParkingManager.ViewModels
{
    public class SpaceReservationViewModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public int SpaceId { get; set; }

        public DateTime BeginsAt { get; set; }

        public DateTime EndsAt { get; set; }

        public string SpaceMark { get; set; }
    }
}