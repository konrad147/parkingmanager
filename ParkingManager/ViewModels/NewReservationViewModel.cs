﻿using ParkingManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ParkingManager.ViewModels
{
    public class NewReservationViewModel : IValidatableObject
    {
        public DateTime EndsAt { get; set; }
        public DateTime BeginsAt { get; set; }
        public VehicleType VehicleType { get; set; }        

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (BeginsAt >= EndsAt)
            {
                yield return new ValidationResult("End date cannot be equal or later than the start date.",
                                                  new string[] { "Ends"});
            }
        }
    }
}